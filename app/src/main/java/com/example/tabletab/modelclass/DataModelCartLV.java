package com.example.tabletab.modelclass;

public class DataModelCartLV {

    String itemName, itemType;
    Float price;
    int index, quantity, itemId;

    public DataModelCartLV(String itemName, String itemType, Float price, int index, int quantity, int itemId) {
        this.itemName = itemName;
        this.price = price;
        this.itemType = itemType;
        this.index = index;
        this.quantity = quantity;
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public Float getPrice() {
        return price;
    }

    public String getItemType() {
        return itemType;
    }

    public int getItemId() {
        return itemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getIndex() {
        return index;
    }
}

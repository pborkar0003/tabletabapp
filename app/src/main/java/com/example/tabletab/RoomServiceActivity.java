package com.example.tabletab;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.tabletab.sqlite.Sqlite;
import com.example.tabletab.utils.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RoomServiceActivity extends Activity {
    Context ctx;
    Sqlite sqliteVar;
    AppController appCtrlObj = new AppController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ctx = getApplicationContext();
        setContentView(R.layout.room_service_screen);
        sqliteVar = new Sqlite(getApplicationContext());
        Button btnCallRoomService = (Button) findViewById(R.id.btnCallRoomService);

        btnCallRoomService.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                JSONObject objJson = new JSONObject();
                JSONObject tempJson = new JSONObject();
                JSONArray arrJson = new JSONArray();
                String data = new String();
                try {
                    tempJson.put("room_service", "Message");
                    arrJson.put(0, tempJson);
                    objJson.put("details", arrJson);
                    objJson.put("intended_user", "Kitchen_Captain");
                    objJson.put("roomno", "room_001");//getFrom tab credentials
                    objJson.put("type", "FOOD_ORDER");
                    data = objJson.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                sqliteVar.open();
                appCtrlObj.postRequestBuy(getApplicationContext(), data, sqliteVar.getDataFromTabDetails("domain_name") + "/order");
                sqliteVar.close();
            }
        });
    }
}

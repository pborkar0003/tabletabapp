package com.example.tabletab;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tabletab.adapters.CustomAdapterCartLV;
import com.example.tabletab.adapters.CustomAdapterGridView;
import com.example.tabletab.adapters.CustomAdapterExpandableLV;
import com.example.tabletab.adapters.CustomAdapterListView;
import com.example.tabletab.sqlite.Sqlite;
import com.example.tabletab.utils.AppController;

import org.json.JSONObject;

import java.util.ArrayList;

public class EMenuActivity extends Activity {
    private static RecyclerView.Adapter adapter, cartAdapter;
    private static RecyclerView recyclerView;
    private static RecyclerView recyclerViewCartItems;
    public static View.OnClickListener myOnClickListener;
    private static ArrayList<Integer> removedItems;
    Context ctx;
    boolean fromInside = false;
    String layoutType = new String();
    AppController appCtrlObj = new AppController();

    public static final String TAG = AppController.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        final Sqlite sqliteVar = new Sqlite(getApplicationContext());
        setContentView(R.layout.emenu_screen);
        Log.i("check", "oncreate");
        ctx = getApplicationContext();
        final MyData myDataObj = new MyData(getApplicationContext());//getApplicationContext()
        myOnClickListener = new MyOnClickListener(this);

        Button btnBuy = (Button) findViewById(R.id.btnBuy);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        //logic to display listview or grid
        String showGridLayout = getIntent().getStringExtra("LAYOUT_TYPE");
        Log.i("showGridLayout : ", showGridLayout);
        Log.i("myDataObj", myDataObj.getCatagory1Details().toString());
        String[] showGridArr = showGridLayout.split("-split-");
        Log.i("check split", showGridArr[1]);
        if (!showGridArr[1].matches("TEST")) {//showGridArr[1]== "TEST"
            Log.i("check", "inside iF");
            sqliteVar.open();
            Log.i("check", "result " + sqliteVar.checkCategory2Present(showGridArr[1]));
            if (sqliteVar.checkCategory2Present(showGridArr[1])) {
                showGridArr[0] = "EXP_LV";
            } else {
                showGridArr[0] = "LV";
            }
            sqliteVar.close();
        }

        setCartItemsRecyclerView(sqliteVar);

        switch (showGridArr[0]) {
            case "GRID":
                recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
                adapter = new CustomAdapterGridView(myDataObj.getCatagory1Details());//gridData
                fromInside = false;
                break;
            case "EXP_LV":
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                adapter = new CustomAdapterExpandableLV(myDataObj.expLvData(showGridArr[1]));
                fromInside = true;
                break;
            case "LV":
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                adapter = new CustomAdapterListView(myDataObj.lvData(showGridArr[1]), this,recyclerViewCartItems);
                fromInside = true;
                break;
        }
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        removedItems = new ArrayList<Integer>();
        recyclerView.setAdapter(adapter);


        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(EMenuActivity.this, "test", Toast.LENGTH_SHORT).show();
                Log.i("jsonHardCoded ", myDataObj.getOrderDetails());
                sqliteVar.open();
                appCtrlObj.postRequestBuy(ctx, myDataObj.getOrderDetails(), sqliteVar.getDataFromTabDetails("domain_name")+"/order");
                sqliteVar.deleteEntireCart();
                sqliteVar.close();
            }
        });
    }

    public void setCartItemsRecyclerView(Sqlite sqliteVar) {
        recyclerViewCartItems = (RecyclerView) findViewById(R.id.rvSelectedCart);
        recyclerViewCartItems.setHasFixedSize(true);
        recyclerViewCartItems.setLayoutManager(new LinearLayoutManager(this));
        sqliteVar.open();
        cartAdapter = new CustomAdapterCartLV(sqliteVar.getCartDetails(), this);
        sqliteVar.close();
        recyclerViewCartItems.setAdapter(cartAdapter);
    }


    private class MyOnClickListener implements View.OnClickListener {
        private final Context context;

        private MyOnClickListener(Context context) {
            this.context = context;
        }

        @Override
        public void onClick(View v) {
            decideLvOrExpLv(v);
        }

        private void decideLvOrExpLv(View v) {
            //get name of the category and based on that populate data
            int selectedItemPosition = recyclerView.getChildAdapterPosition(v);
            RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(selectedItemPosition);

            TextView textViewName = (TextView) viewHolder.itemView.findViewById(R.id.textViewName);
            String selectedName = (String) textViewName.getText().toString();
            // Toast.makeText(context, selectedItemPosition + " " + selectedName, Toast.LENGTH_SHORT).show();
            if (selectedItemPosition % 2 == 0) {
                layoutType = "LV";
            } else {
                layoutType = "EXP_LV";
            }
            layoutType = layoutType + "-split-" + selectedName;
            Intent i = new Intent(EMenuActivity.this, EMenuActivity.class);
            i.putExtra("LAYOUT_TYPE", layoutType);
            startActivity(i);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == R.id.add_item) {
            Toast.makeText(this, "test 2", Toast.LENGTH_SHORT).show();
            //check if any items to add
            if (removedItems.size() != 0) {
                //addRemovedItemToList();
            } else {
                Toast.makeText(this, "Nothing to add", Toast.LENGTH_SHORT).show();
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (fromInside) {
            Intent i = new Intent(EMenuActivity.this, EMenuActivity.class);
            i.putExtra("LAYOUT_TYPE", "GRID-split-TEST");
            startActivity(i);
            finish();
        } else {
            Intent i = new Intent(EMenuActivity.this, HomeActivity.class);
            startActivity(i);
            finish();
        }
    }
}

package com.example.tabletab;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class PlacesToVisit extends Activity{
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ctx = getApplicationContext();
        setContentView(R.layout.popular_ad_view_screen);
        WebView webView = (WebView) findViewById(R.id.webViewpopularAd);
        webView.setWebViewClient(new WebViewClient());

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("https://www.tripadvisor.in/Attractions-g297604-Activities-Goa.html");
        webView.setHorizontalScrollBarEnabled(false);

    }
}

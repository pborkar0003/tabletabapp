package com.example.tabletab.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tabletab.EMenuActivity;
import com.example.tabletab.R;
import com.example.tabletab.modelclass.DataModelLV;
import com.example.tabletab.sqlite.Sqlite;

import java.util.ArrayList;
import java.util.List;

public class CustomAdapterListView extends RecyclerView.Adapter<CustomAdapterListView.ViewHolder> {
    private ArrayList<DataModelLV>  values;
    Context context;
    RecyclerView recyclerViewCartItems;
    EMenuActivity varTemp = new EMenuActivity();

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtHeader;
        public TextView txtFooter;
        public TextView type;
        public Button selectItem;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            txtHeader = (TextView) v.findViewById(R.id.sequence);
            txtFooter = (TextView) v.findViewById(R.id.childItem);
            type = (TextView) v.findViewById(R.id.type);
            selectItem = (Button) v.findViewById(R.id.btnSelectItem);
        }
    }

//    public void add(int position, ArrayList<DataModelLV> item) {
//        values.add(position,item);
//        notifyItemInserted(position);
//    }


    // Provide a suitable constructor (depends on the kind of dataset)
    public CustomAdapterListView(ArrayList<DataModelLV> myDataset, Context c,RecyclerView cartRecView) {
        values = myDataset;
        context = c;
        recyclerViewCartItems= cartRecView;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CustomAdapterListView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.child_item_expandable_lv, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //String name = values.get(position);
        holder.txtHeader.setText(values.get(position).getItemName());
        holder.type.setText(values.get(position).getType());
        holder.txtFooter.setText(values.get(position).getPrice().toString());
        holder.txtHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Views", "Clicked on view");
                //remove(position);
            }
        });
        final String itemName = holder.txtHeader.getText().toString();
        final Double itemPrice = Double.parseDouble(holder.txtFooter.getText().toString());
        final String itemType = holder.type.getText().toString();
        final int itemId = values.get(position).getItemId();

        final Sqlite sqliteVar = new Sqlite(context);

        holder.selectItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RecyclerView.Adapter cartAdapter;

//                recyclerViewCartItems = (RecyclerView) holder.findViewById(R.id.rvSelectedCart);
//                recyclerViewCartItems.setHasFixedSize(true);
//                recyclerViewCartItems.setLayoutManager(new LinearLayoutManager(context));

                sqliteVar.open();
                sqliteVar.addToCart(itemId,itemName,itemPrice,itemType);
                cartAdapter = new CustomAdapterCartLV(sqliteVar.getCartDetails(), context);
                sqliteVar.close();
                recyclerViewCartItems.setAdapter(cartAdapter);
                cartAdapter.notifyDataSetChanged();
//                varTemp.setCartItemsRecyclerView(sqliteVar);
                Toast.makeText(context, itemName +" added to your cart " ,Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return values.size();
    }

}
package com.example.tabletab;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tabletab.modelclass.DataModelKitchenCaptainLV;
import com.example.tabletab.sqlite.Sqlite;
import com.example.tabletab.utils.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends Activity {
    Context ctx;
    AppController appCtrlObj = new AppController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ctx = getApplicationContext();
        final Sqlite sqliteVar = new Sqlite(ctx);
        sqliteVar.open();
        final String userType = sqliteVar.showLogiInLayout(); //"TAB";//sqliteVar.showLogiInLayout();;//""KITCHEN_CAPTAIN
        sqliteVar.close();
        Intent i;
        final AppController appCtrlObj = new AppController();
        Log.i("mainactivity", userType);

        switch (userType) {
            //open home screen
            case "TAB":
                i = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
                break;
            case "KITCHEN_CAPTAIN":
                i = new Intent(MainActivity.this, KitchenCaptianActivity.class);
                startActivity(i);
                finish();
                break;
            default:
                //open login layout
                setContentView(R.layout.login_screen);

                final EditText tabId = (EditText) findViewById(R.id.edtRoomNum);
                final EditText password = (EditText) findViewById(R.id.edtPassword);
                final EditText ipAddress = (EditText) findViewById(R.id.edtIpAddress);
                Button login = (Button) findViewById(R.id.btnLogIn);

                login.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        appCtrlObj.postRequest(ctx, tabId.getText().toString(), password.getText().toString(), ipAddress.getText().toString() + "/login", new VolleyCallback() {
                            String userType = "";
                            JSONObject obj = new JSONObject();
                            String token = "";
                            String roomNumber = "";

                            @Override
                            public void onSuccessKc(ArrayList<DataModelKitchenCaptainLV> result) {

                            }

                            @Override
                            public void onSuccess(String result) {
                                try {
                                    obj = new JSONObject(result);
                                    Log.d("My App", obj.toString());

                                } catch (Throwable t) {
                                    Log.e("My App", "Could not parse malformed JSON: \"" + result + "\"");
                                }

                                try {
                                    Log.i("callack", obj.getString("message"));
                                    if (obj.getString("message").matches("ok")) {
                                        JSONObject dataObj = obj.getJSONObject("data");
                                        dataObj = dataObj.getJSONObject("user");
                                        userType = dataObj.getString("role");
                                        // roomNumber = dataObj.getString("roomno");

                                        token = dataObj.getString("token");
                                        Log.i("token", token);
                                    }else{
                                        Toast.makeText(ctx,"Wrong message nigga",Toast.LENGTH_LONG).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                //sqliteVar.open();
                                //String userType = sqliteVar.insertLoginCred(roomNum.getText().toString(), password.getText().toString());
                                //sqliteVar.close();
                                Intent i;
                                Log.i("callackkkkd", userType);
                                switch (userType) {
                                    //open home screen
                                    case "TAB":
                                        sqliteVar.open();//if login credentials are okay than push into the database
                                        sqliteVar.insertTabDetails(tabId.getText().toString(), ipAddress.getText().toString(), userType, token, roomNumber);
                                        sqliteVar.close();
                                        i = new Intent(MainActivity.this, HomeActivity.class);
                                        startActivity(i);
                                        finish();
                                        break;
                                    case "KITCHEN_CAPTAIN":
                                        i = new Intent(MainActivity.this, KitchenCaptianActivity.class);
                                        startActivity(i);
                                        finish();
                                        break;
                                    default:
                                        //open login layout
                                        //Toast.makeText(this,"Error while log in",Toast.LENGTH_LONG).show();
                                        setContentView(R.layout.login_screen);
                                        break;
                                }
                            }
                        });
                    }
                });
                break;
        }
    }
}

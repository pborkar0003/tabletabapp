package com.example.tabletab;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tabletab.sqlite.Sqlite;
import com.example.tabletab.utils.AppController;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.Date;

public class HomeActivity extends Activity implements View.OnClickListener {
    CarouselView carouselView;
    Context ctx;
    AppController appCtrlObj = new AppController();
    int[] sampleImages = {R.drawable.home_waiter, R.drawable.logo, R.drawable.tenor};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ctx = getApplicationContext();
        setContentView(R.layout.home_screen);
        TextView day = (TextView) findViewById(R.id.tvDay);
        TextView tvDate = (TextView) findViewById(R.id.tvDate);
        Button eMenu = (Button) findViewById(R.id.btnEMenu);
        Button places = (Button) findViewById(R.id.btnPlaces);
        Button popular = (Button) findViewById(R.id.btnPopular);
        Button roomService = (Button) findViewById(R.id.btnRoomService);
        Button guestMessaging = (Button) findViewById(R.id.btnGuestMessaging);
        Button btnAlarm = (Button) findViewById(R.id.btnAlarm);
        Button btnHotelMap = (Button) findViewById(R.id.btnHotelMap);
        Button btnService = (Button) findViewById(R.id.btnService);
        Button btnEPaper = (Button) findViewById(R.id.btnEPaper);

        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleImages.length);

        carouselView.setImageListener(imageListener);

        Date d = new Date();
        day.setText(android.text.format.DateFormat.format("EEE", d));
        tvDate.setText(android.text.format.DateFormat.format("d MMM yyyy", d));
        //time.setText(android.text.format.DateFormat.format("h:mm a", d));
        Sqlite sqliteVar = new Sqlite(getApplicationContext());
        sqliteVar.open();
        if (sqliteVar.shouldFetchemenuData()) {
            ProgressDialog pDialog = new ProgressDialog(this);
            pDialog.setMessage("Fetching Data...");
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
            appCtrlObj.postRequestWithHeaders(ctx, pDialog);
        }
        sqliteVar.close();

        eMenu.setOnClickListener(this);
        roomService.setOnClickListener(this);
        places.setOnClickListener(this);
        popular.setOnClickListener(this);
        guestMessaging.setOnClickListener(this);
        btnAlarm.setOnClickListener(this);
        btnHotelMap.setOnClickListener(this);
        btnService.setOnClickListener(this);
        btnEPaper.setOnClickListener(this);
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };

    public void onClick(View varb) {
        Intent i = new Intent();
        switch (varb.getId()) {
            case R.id.btnEMenu:
                i = new Intent(HomeActivity.this, EMenuActivity.class);
                i.putExtra("LAYOUT_TYPE", "GRID-split-TEST");
                startActivity(i);
                break;
            case R.id.btnPlaces:
                i = new Intent(HomeActivity.this, PlacesToVisit.class);
                i.putExtra("LAYOUT_TYPE", "GRID-split-TEST");
                startActivity(i);
                break;
            case R.id.btnPopular:
                i = new Intent(HomeActivity.this, PopularAdViewActivity.class);
                i.putExtra("LAYOUT_TYPE", "GRID-split-TEST");
                startActivity(i);
                break;
            case R.id.btnRoomService:
                i = new Intent(HomeActivity.this, RoomServiceActivity.class);
                i.putExtra("LAYOUT_TYPE", "GRID-split-TEST");
                startActivity(i);
                break;
            case R.id.btnGuestMessaging:
                i = new Intent(HomeActivity.this, MessagingActivity.class);
                i.putExtra("LAYOUT_TYPE", "GRID-split-TEST");
                startActivity(i);
                break;
            case R.id.btnAlarm:
                i = new Intent(AlarmClock.ACTION_SET_ALARM);
                startActivity(i);
                break;
            case R.id.btnHotelMap:
                String uri = "http://maps.google.com/maps?q=loc:" + 15.283219 + "," + 73.986191 + " (test title)";
                i = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(i);
                break;
            case R.id.btnService:
                Toast.makeText(this,"Service",Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnEPaper:
                Toast.makeText(this,"E Paper",Toast.LENGTH_SHORT).show();
                break;
        }

    }

    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }


}

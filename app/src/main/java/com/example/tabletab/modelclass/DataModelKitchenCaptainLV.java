package com.example.tabletab.modelclass;

public class DataModelKitchenCaptainLV {
    String itemName, type;
    int itemId,qty;

    public DataModelKitchenCaptainLV(String itemName, String type, int qty,int itemId) {
        this.itemName = itemName;
        this.type = type;
        this.qty = qty;
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public String getType() {
        return type;
    }

    public int getQty() {
        return qty;
    }

    public int getItemId(){
        return itemId;
    }
}
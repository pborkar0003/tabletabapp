package com.example.tabletab.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.WindowManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.tabletab.MainActivity;
import com.example.tabletab.VolleyCallback;
import com.example.tabletab.modelclass.DataModelKitchenCaptainLV;
import com.example.tabletab.sqlite.Sqlite;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AppController {

    public static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static AppController mInstance;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

//    public RequestQueue getRequestQueue() {
//        if (mRequestQueue == null) {
//            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
//        }
//
//        return mRequestQueue;
//    }

//    public ImageLoader getImageLoader() {
//        getRequestQueue();
//        if (mImageLoader == null) {
//            mImageLoader = new ImageLoader(this.mRequestQueue,
//                    new LruBitmapCache());
//        }
//        return this.mImageLoader;
//    }

//    public <T> void addToRequestQueue(Request<T> req, String tag) {
//        // set the default tag if tag is empty
//        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
//        getRequestQueue().add(req);
//    }
//
//    public <T> void addToRequestQueue(Request<T> req) {
//        req.setTag(TAG);
//        getRequestQueue().add(req);
//    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void postRequestWithHeaders(Context ctx, final ProgressDialog pDialog) {
        Log.i("network", "postrequest");
        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        //https://api.myjson.com/bins/1eszvr
        final Sqlite sqliteObj = new Sqlite(ctx);
        sqliteObj.open();
        String url = sqliteObj.getDataFromTabDetails("domain_name")+"/emenu";
        sqliteObj.close();

        JsonArrayRequest jsonArrayReq = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i(TAG, response.toString());
                sqliteObj.open();
                JSONObject obj = new JSONObject();
                // Parsing json
                for (int i = 0; i < response.length(); i++) {
                    try {
                        obj = response.getJSONObject(i);
                        sqliteObj.addToEMenuDatabase(obj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                sqliteObj.close();
                pDialog.dismiss();
                //adapter.notifyDataSetChanged();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Network", "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                sqliteObj.open();
                Log.i("test token", sqliteObj.getDataFromTabDetails("token"));
                Map<String, String> params = new HashMap<>();

                params.put("Authorization", "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6InRhYl8wMDEiLCJyb2xlIjoiVEFCIiwiaWF0IjoxNTE0MTc5NDUwfQ.CRqfF4uyXpX0wZkiR81LT5S7a6SNXhpwaMu_vnFg270");
                sqliteObj.close();
                //..add other headers
                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(ctx);
        //adding the string request to request queue
        requestQueue.add(jsonArrayReq);
        // Adding request to request queue
        // AppController.getInstance().addToRequestQueue(searchMsg);
    }

    public void postRequest(Context ctx, final String id, final String key, String url, final VolleyCallback callback) {
        //url = "http://httpbin.org/post";
        // String responseData;
//        final ProgressDialog pDialog = new ProgressDialog(ctx);
//        pDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
//        pDialog.setMessage("Loging in...");
//        pDialog.show();]

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.i("Response", response);
                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("Error.Response", error.toString());

                        String responseData = new String("invalid ip address");
                        int statCode = 0;
                        try {
                            responseData = new String(error.networkResponse.data);
                            statCode = error.networkResponse.statusCode;
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        Log.i("Error.Response status ", Integer.toString(statCode));
                        //Log.i("Error.Response",responseData);
                        callback.onSuccess(responseData);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                //params = postParamJsonObject;
                params.put("id", id);
                params.put("key", key);
                return params;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                int mStatusCode = response.statusCode;
                Log.i("statusCode", "parseNetworkResponse: " + mStatusCode);
                return super.parseNetworkResponse(response);
            }
        };
        RequestQueue queue = Volley.newRequestQueue(ctx);
        queue.add(postRequest);
    }

    public void postRequestBuy(Context ctx, final String postParamJsonObject, String url) {
        final Sqlite sqliteObj = new Sqlite(ctx);

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.i("Response", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.i("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                sqliteObj.open();
                Log.i("test token", sqliteObj.getDataFromTabDetails("token"));
                Map<String, String> params = new HashMap<>();

                params.put("Authorization", "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6InUiLCJyb2xlIjoiVEFCIiwiaWF0IjoxNTE0MTc3NDM1fQ.2OlQXObov7c7KaMV1p-wMpPcCZ10OPMS9k0jnT-YRKo");
                sqliteObj.close();
                //..add other headers
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("order", postParamJsonObject);
                return params;
            }
        };
        RequestQueue queue = Volley.newRequestQueue(ctx);
        queue.add(postRequest);
    }


    public void kcGetData(Context ctx, final VolleyCallback callback) {
        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";
        final ArrayList<DataModelKitchenCaptainLV> input;
        input = new ArrayList<DataModelKitchenCaptainLV>();

        String url = "https://api.myjson.com/bins/haxwj";
        final Sqlite sqliteObj = new Sqlite(ctx);

        JsonArrayRequest jsonArrayReq = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i(TAG, response.toString());
                JSONObject obj = new JSONObject();
                // Parsing json
                for (int i = 0; i < response.length(); i++) {
                    try {
                        obj = response.getJSONObject(i);
                        JSONArray arr = obj.getJSONArray("details");
//                        for (int j = 0; j < arr.length(); j++) {
//
//                        }
                        JSONObject k = new JSONObject();
                        k = arr.getJSONObject(0);
                        JSONObject l = k.getJSONObject("food_item");
                        input.add(new DataModelKitchenCaptainLV(l.getString("name"), l.getString("type"), k.getInt("quantity"), 1));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                callback.onSuccessKc(input);
                //adapter.notifyDataSetChanged();
                //pDialog.dismiss();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Network", "Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                sqliteObj.open();
                Log.i("test token", sqliteObj.getDataFromTabDetails("token"));
                Map<String, String> params = new HashMap<>();

                params.put("Authorization", "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6InRhYl8wMDEiLCJyb2xlIjoiVEFCIiwiaWF0IjoxNTE0MTc5NDUwfQ.CRqfF4uyXpX0wZkiR81LT5S7a6SNXhpwaMu_vnFg270");
                sqliteObj.close();
                //..add other headers
                return params;
            }
        };
        //creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(ctx);
        //adding the string request to request queue
        requestQueue.add(jsonArrayReq);
        // Adding request to request queue
        // AppController.getInstance().addToRequestQueue(searchMsg);
    }
}
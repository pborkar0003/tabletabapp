package com.example.tabletab;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.tabletab.adapters.CustomAdapterCartLV;
import com.example.tabletab.adapters.CustomAdapterKitchenCaptainListView;
import com.example.tabletab.modelclass.DataModelKitchenCaptainLV;
import com.example.tabletab.utils.AppController;

import java.util.ArrayList;

public class KitchenCaptianActivity extends Activity {
    AppController appCtrlObj = new AppController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.kitchen_captain_screen);

        final RecyclerView recyclerViewCartItems = (RecyclerView) findViewById(R.id.rcKitchenCaptain);
        recyclerViewCartItems.setHasFixedSize(true);
        recyclerViewCartItems.setLayoutManager(new LinearLayoutManager(this));
        MyData myDataObj = new MyData(getApplicationContext());                  //setWithEmptyData
        final Context ctxx = this;
        final ProgressDialog pDia = new ProgressDialog(this);
        pDia.show();
        appCtrlObj.kcGetData(getApplicationContext(), new VolleyCallback() {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onSuccessKc(ArrayList<DataModelKitchenCaptainLV> result) {
                RecyclerView.Adapter kcAdapter = new CustomAdapterKitchenCaptainListView(result, ctxx);
                recyclerViewCartItems.setAdapter(kcAdapter);
                pDia.dismiss();
            }
        });

        //callTheAppaControllerMethod

    }
}

package com.example.tabletab.modelclass;

public class DataModelLV {
    String itemName, type;
    Float price;
    int itemId;

    public DataModelLV(String itemName, String type, Float price,int itemId) {
        this.itemName = itemName;
        this.type = type;
        this.price = price;
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public String getType() {
        return type;
    }

    public Float getPrice() {
        return price;
    }

    public int getItemId(){
        return itemId;
    }
}

package com.example.tabletab.sqlite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.example.tabletab.modelclass.DataModelCartLV;
import com.example.tabletab.modelclass.DataModelGrid;
import com.example.tabletab.modelclass.DataModelLV;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class Sqlite {
    static String DB_NAME = "tableTab";
    static int DB_VER = 1;
    static String DB_PATH;

    DBhelper helper;
    SQLiteDatabase database;
    Context context;

    class DBhelper extends SQLiteOpenHelper {

        public DBhelper(Context context) {
            super(context, DB_NAME, null, DB_VER);
            // TODO Auto-generated constructor stub
        }

        @Override
        public void onCreate(SQLiteDatabase arg0) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
            // TODO Auto-generated method stub

        }

        public void createDatabase() {
            // TODO Auto-generated method stub
            boolean pathExist = checkDB();
            if (pathExist) {// do nothing
                Log.i("TAG", "database Exists");
            } else {
                this.getReadableDatabase();
                try {
                    CopyDataBase();

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

        }

        private void CopyDataBase() throws IOException {

            Log.i("TAG", "Copying Database.");

            InputStream myInput = context.getAssets().open(DB_NAME + ".sqlite");
            String outFileName = DB_PATH + "";

            Log.i("TAG", "output file path:" + DB_PATH);

            OutputStream myOutput = new FileOutputStream(outFileName);

            byte[] buffer = new byte[1024];
            int length;

            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }

            // Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();
            Log.i("TAG", "Finished copying.");
        }

        private boolean checkDB() {
            // TODO Auto-generated method stub
            boolean dbBool = false;
            String path = context.getFilesDir().getAbsolutePath().replace("files", "databases") + File.separator + DB_NAME;
            File Fpath = new File(path);
            dbBool = Fpath.exists();
            return dbBool;
        }
    }

    public Sqlite(Context c) {
        context = c;
        DB_PATH = context.getDatabasePath(DB_NAME).getAbsolutePath();
        DBhelper helper = new DBhelper(context);
        helper.createDatabase();
    }

    public void open() {
        helper = new DBhelper(context);
        database = helper.getWritableDatabase();
    }

    public void close() {
        helper.close();
    }

    public String showLogiInLayout() {
        String queryCheckAvailibity = "SELECT role from tab_details";
        Cursor cursor = database.rawQuery(queryCheckAvailibity, null);
        cursor.moveToLast();
        int n = cursor.getPosition();  //use it to return values to open registration activity
        if (n == -1) {
            return "";
        } else {
            Log.i("dbOperations", cursor.getString(0));
            return cursor.getString(0);
        }
    }

    public void insertTabDetails(String username, String domain_name, String role, String token, String roomNo) {
//        userType = "";
//        if (username.toLowerCase().contains("cook")) {
//            userType = "cook";
//        } else {
//            userType = "room";
//        }
        String queryFinalBalance = "SELECT username FROM tab_details";
        Cursor cursor = database.rawQuery(queryFinalBalance, null);

        cursor.moveToLast();
        int n = cursor.getPosition();
        if (n == -1) {
            String query = "insert into tab_details(username,domain_name,role,token,room_no) values('" + username + "','" + domain_name + "','" + role + "','" + token + "','" + roomNo + "')";
            database.execSQL(query);
            Log.i("dbOperations", "tab details data inserted into database");
        }
        // return userType;
    }

    public void addToCart(int itemId, String itemName, double price, String itemType) {
        String query = "insert into cart(item_id,item_name,item_price,item_type) values('" + itemId + "','" + itemName + "','" + price + "','" + itemType + "')";
        database.execSQL(query);
        Log.i("dbOperations", "data inserted into cart database");
    }

    public void deleteEntireCart(){
        String query = "DELETE from cart";
        database.execSQL(query);
        Log.i("dbOperations","table deleted");
    }

    public ArrayList<DataModelCartLV> getCartDetails() {
        ArrayList<DataModelCartLV> input;
        input = new ArrayList<DataModelCartLV>();

        String queryCheckAvailibity = "SELECT * from cart";
        Cursor cursor = database.rawQuery(queryCheckAvailibity, null);
        cursor.moveToLast();
        cursor.moveToFirst();

        if (cursor.moveToFirst()) {
            int i = 0;
            do {
                Log.d("dbOperations", "inside the cursors do while loop " + i);
                input.add(new DataModelCartLV(cursor.getString(cursor.getColumnIndex("item_name")), cursor.getString(cursor.getColumnIndex("item_type")), cursor.getFloat(cursor.getColumnIndex("item_price")), cursor.getInt(cursor.getColumnIndex("ind")), cursor.getInt(cursor.getColumnIndex("quantity")), cursor.getInt(cursor.getColumnIndex("item_id"))));
                i++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return input;
    }

    public void addToEMenuDatabase(JSONObject obj) {
        try {
            JSONArray imgArray = obj.getJSONArray("img");
            Log.i("tes", imgArray.getString(0));
            String query = "insert into eMenu(id,name,price,type,category1,category2,description,img) values('" + obj.getString("id") + "','" + obj.getString("name") + "','" + obj.getString("price").replaceAll("[^\\d.]", "") + "','" + obj.getString("type") + "','" + obj.getString("category1") + "','" + obj.getString("category2") + "','" + obj.getString("description") + "','" + imgArray.getString(0).replace("\\", "/") + "')";
            database.execSQL(query);
            Toast.makeText(context, "Data inserted", Toast.LENGTH_SHORT);
            Log.i("dbOperations", "Emenu database inserted");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<DataModelGrid> getCatagory1Details() {
        ArrayList<DataModelGrid> input;
        input = new ArrayList<DataModelGrid>();
        String queryCheckAvailibity = "SELECT category1,category2,img from eMenu group by category1";
        Cursor cursor = database.rawQuery(queryCheckAvailibity, null);
        cursor.moveToLast();
        cursor.moveToFirst();
        if (cursor.moveToFirst()) {
            int i = 0;
            do {

                Log.d("image dynamic", cursor.getString(2) + i);
                if (cursor.getString(1) == " " || cursor.getString(1) == null) {
                    input.add(new DataModelGrid(cursor.getString(0), cursor.getString(2), 0, 0));

                } else {
                    input.add(new DataModelGrid(cursor.getString(0), cursor.getString(2), 1, 0));
                }

                i++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return input;
    }

    public ArrayList<DataModelLV> getSelectedCategoryLV(String category1) {
        ArrayList<DataModelLV> input;
        input = new ArrayList<DataModelLV>();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT name,type,price,id from eMenu where category1 =");
        stringBuilder.append("'" + category1 + "'");
        String queryCheckAvailibity = stringBuilder.toString();
        Cursor cursor = database.rawQuery(queryCheckAvailibity, null);
        cursor.moveToLast();
        cursor.moveToFirst();
        if (cursor.moveToFirst()) {
            int i = 0;
            do {
                Log.d("dbOperations", "inside the cursors do while loop " + i);
                input.add(new DataModelLV(cursor.getString(0), cursor.getString(1), cursor.getFloat(2), cursor.getInt(3)));
                i++;
            } while (cursor.moveToNext());
        }
        cursor.close();
        return input;
    }

    public Boolean checkCategory2Present(String category1) {
        Log.i("check cate 2:", category1);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT category2 from eMenu where category1 =");
        stringBuilder.append("'" + category1 + "'");
        String queryCheckAvailibity = stringBuilder.toString();
        Cursor cursor = database.rawQuery(queryCheckAvailibity, null);
        cursor.moveToFirst();
        //Boolean test = false;
        if (cursor != null && cursor.moveToFirst()) {
            Log.i("check", "result db " + cursor.getString(0));
            if (cursor.getString(0).matches("") || cursor.getString(0).matches(" ")) {
                cursor.close();
                return false;
            } else {
                cursor.close();
                return true;
            }
        } else {
            return false;
        }
        //return test;
    }

    public String getDataFromTabDetails(String columnName) {
        String queryCheckAvailibity = "SELECT " + columnName + " from tab_details";
        Cursor cursor = database.rawQuery(queryCheckAvailibity, null);
        cursor.moveToLast();
        int n = cursor.getPosition();
        if (n == -1) {
            return "";
        } else {
            Log.i("dbOperations", cursor.getString(0));
            return cursor.getString(0);
        }
    }

    public void updateQuantityInCart(int qty, int index) {
        String query = "UPDATE cart SET quantity =" + qty + " where ind ="+index+" ";
        Log.i("dbOperations", query);
        database.execSQL(query);
        Log.i("dbOperations", query);
    }

    public void deleteFromCart(int index) {
        String query = "DELETE FROM cart where ind ="+index+" ";
        Log.i("dbOperations", query);
        database.execSQL(query);
        Log.i("dbOperations", query);
    }

    public boolean shouldFetchemenuData(){
        String queryCheckAvailibity = "SELECT * from eMenu";
        Cursor cursor = database.rawQuery(queryCheckAvailibity, null);
        cursor.moveToLast();
        int n = cursor.getPosition();
        if (n == -1) {
            return true;
        } else {
            Log.i("dbOperations", cursor.getString(0));
            return false;
        }
    }
}


package com.example.tabletab;

import com.example.tabletab.modelclass.DataModelKitchenCaptainLV;

import java.util.ArrayList;

public interface VolleyCallback {
    void onSuccess(String result);
    void onSuccessKc(ArrayList<DataModelKitchenCaptainLV> result);
}

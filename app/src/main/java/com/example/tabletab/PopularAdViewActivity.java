package com.example.tabletab;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class PopularAdViewActivity extends Activity {
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        ctx = getApplicationContext();
        setContentView(R.layout.popular_ad_view_screen);
        WebView webView = (WebView) findViewById(R.id.webViewpopularAd);
        webView.setWebViewClient(new WebViewClient());

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://google.com");
        webView.setHorizontalScrollBarEnabled(false);

    }
}

//private class MyWebViewClient extends WebViewClient {
//    @Override
//    public boolean shouldOverrideUrlLoading(WebView view, String url) {
//        if (Uri.parse(url).getHost().equals("www.example.com")) {
//            // Designate Urls that you want to load in WebView still.
//            return false;
//        }
//        // Otherwise, give the default behavior (open in browser)
//        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//        startActivity(intent);
//        return true;
//    }
//}

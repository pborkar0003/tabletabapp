package com.example.tabletab.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tabletab.R;
import com.example.tabletab.modelclass.DataModelCartLV;
import com.example.tabletab.modelclass.DataModelLV;
import com.example.tabletab.sqlite.Sqlite;

import java.util.ArrayList;
import java.util.List;

public class CustomAdapterCartLV extends RecyclerView.Adapter<CustomAdapterCartLV.ViewHolder> {
    private ArrayList<DataModelCartLV> values;
    Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtHeader;
        public TextView txtFooter;
        public Button selectItem;
        public EditText edtQty;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            txtHeader = (TextView) v.findViewById(R.id.tvItemNameCart);
            txtFooter = (TextView) v.findViewById(R.id.tvPriceCart);
            edtQty = (EditText) v.findViewById(R.id.etQtyCart);
            selectItem = (Button) v.findViewById(R.id.btnDelete);
        }
    }

//    public void add(int position, String item) {
//        values.add(position, item);
//        notifyItemInserted(position);
//    }


    // Provide a suitable constructor (depends on the kind of dataset)
    public CustomAdapterCartLV(ArrayList<DataModelCartLV> myDataset, Context c) {
        values = myDataset;
        context = c;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CustomAdapterCartLV.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.cart_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.txtHeader.setText(values.get(position).getItemName());
        holder.txtHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Views", "Clicked on view");
                //remove(position);
            }
        });
        holder.txtFooter.setText(values.get(position).getPrice().toString());
        final String itemName = holder.txtHeader.getText().toString();
        final Double itemPrice = Double.parseDouble(holder.txtFooter.getText().toString());
        final int qty = Integer.parseInt(holder.edtQty.getText().toString());
        final Sqlite sqliteVar = new Sqlite(context);

        holder.selectItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, itemName + " Removed from cart", Toast.LENGTH_SHORT).show();
                sqliteVar.open();
                sqliteVar.deleteFromCart(Integer.parseInt(Integer.toString(values.get(position).getIndex())));
                sqliteVar.close();
                remove(position);
            }
        });

        holder.edtQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //write your logic to update the quantity here
                Log.i("test text watcher",Integer.toString(values.get(position).getIndex()));//call update method in sqlite version
                String temp = Integer.toString(values.get(position).getIndex());
                if (!editable.toString().equals("") || !editable.toString().equals(null)) {
                    sqliteVar.open();
                    sqliteVar.updateQuantityInCart(Integer.parseInt(editable.toString()), Integer.parseInt(temp));
                    sqliteVar.close();
                    Log.i("test text watcher", editable.toString());
                }
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return values.size();
    }

    public void remove(int position) {
        values.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, values.size());
    }

}
package com.example.tabletab.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tabletab.EMenuActivity;
import com.example.tabletab.R;
import com.example.tabletab.modelclass.DataModelKitchenCaptainLV;
import com.example.tabletab.modelclass.DataModelLV;
import com.example.tabletab.sqlite.Sqlite;

import java.util.ArrayList;

public class CustomAdapterKitchenCaptainListView extends RecyclerView.Adapter<CustomAdapterKitchenCaptainListView.ViewHolder> {
    private ArrayList<DataModelKitchenCaptainLV> values;
    Context context;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvKcItemName;
        public TextView tvKcQty;
        public TextView tvKctype;
        public Button selectItem;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            tvKcItemName = (TextView) v.findViewById(R.id.tvKcNameOfItem);
            tvKcQty = (TextView) v.findViewById(R.id.tvKcType);
            tvKctype = (TextView) v.findViewById(R.id.tvKcQty);
            selectItem = (Button) v.findViewById(R.id.btnKcAcknowledge);
        }
    }

//    public void add(int position, ArrayList<DataModelLV> item) {
//        values.add(position,item);
//        notifyItemInserted(position);
//    }


    // Provide a suitable constructor (depends on the kind of dataset)
    public CustomAdapterKitchenCaptainListView(ArrayList<DataModelKitchenCaptainLV> myDataset, Context c) {
        values = myDataset;
        context = c;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CustomAdapterKitchenCaptainListView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.kitchen_captain_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        CustomAdapterKitchenCaptainListView.ViewHolder vh = new CustomAdapterKitchenCaptainListView.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(CustomAdapterKitchenCaptainListView.ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //String name = values.get(position);
        holder.tvKcItemName.setText(values.get(position).getItemName());
        holder.tvKctype.setText(values.get(position).getType());
        holder.tvKcQty.setText("0");
        holder.tvKcItemName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Views", "Clicked on view");
                //remove(position);
            }
        });

        holder.selectItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Ackowledged order",Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return values.size();
    }

}
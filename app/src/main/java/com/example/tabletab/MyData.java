package com.example.tabletab;

import android.content.Context;

import com.example.tabletab.adapters.CustomAdapterExpandableLV;
import com.example.tabletab.modelclass.DataModelCartLV;
import com.example.tabletab.modelclass.DataModelGrid;
import com.example.tabletab.modelclass.DataModelKitchenCaptainLV;
import com.example.tabletab.modelclass.DataModelLV;
import com.example.tabletab.sqlite.Sqlite;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyData {
    Context context;

    public MyData(Context c) {//Context c
        this.context = c;
    }

    //    Sqlite sqliteVar = new Sqlite(context);//
    static String[] nameArray = {"Cupcake", "Donut", "Eclair", "Froyo", "Gingerbread", "Honeycomb", "Ice Cream Sandwich", "JellyBean", "Kitkat", "Lollipop", "Marshmallow"};
    static String[] versionArray = {"http://www.westwoodtavern.com/wp-content/uploads/galleries/post-23/thumbnails/westwood-food-web0009-c62.jpg", "https://media-cdn.tripadvisor.com/media/photo-s/0e/07/1e/40/live-saxophonist-great.jpg", "http://www.westwoodtavern.com/wp-content/uploads/galleries/post-23/thumbnails/westwood-food-web0009-c62.jpg", "https://media-cdn.tripadvisor.com/media/photo-s/0e/07/1e/40/live-saxophonist-great.jpg", "http://www.westwoodtavern.com/wp-content/uploads/galleries/post-23/thumbnails/westwood-food-web0009-c62.jpg", "https://media-cdn.tripadvisor.com/media/photo-s/0e/07/1e/40/live-saxophonist-great.jpg", "http://www.westwoodtavern.com/wp-content/uploads/galleries/post-23/thumbnails/westwood-food-web0009-c62.jpg", "http://www.westwoodtavern.com/wp-content/uploads/galleries/post-23/thumbnails/westwood-food-web0009-c62.jpg", "https://media-cdn.tripadvisor.com/media/photo-s/0e/07/1e/40/live-saxophonist-great.jpg", "http://www.westwoodtavern.com/wp-content/uploads/galleries/post-23/thumbnails/westwood-food-web0009-c62.jpg", "https://media-cdn.tripadvisor.com/media/photo-s/0e/07/1e/40/live-saxophonist-great.jpg"};

    static Integer[] drawableArray = {R.drawable.home_waiter, R.drawable.home_waiter, R.drawable.home_waiter,
            R.drawable.home_waiter, R.drawable.home_waiter, R.drawable.home_waiter, R.drawable.home_waiter,
            R.drawable.home_waiter, R.drawable.home_waiter, R.drawable.home_waiter, R.drawable.home_waiter};

    static Integer[] id_ = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    public List<CustomAdapterExpandableLV.Item> expLvData(String selectedCat1) {
        List<CustomAdapterExpandableLV.Item> dataExpLv = new ArrayList<>();
        dataExpLv.add(new CustomAdapterExpandableLV.Item(CustomAdapterExpandableLV.HEADER, "Fruits"));
        dataExpLv.add(new CustomAdapterExpandableLV.Item(CustomAdapterExpandableLV.CHILD, "Apple"));
        dataExpLv.add(new CustomAdapterExpandableLV.Item(CustomAdapterExpandableLV.CHILD, "Orange"));
        dataExpLv.add(new CustomAdapterExpandableLV.Item(CustomAdapterExpandableLV.CHILD, "Banana"));
        dataExpLv.add(new CustomAdapterExpandableLV.Item(CustomAdapterExpandableLV.HEADER, "Cars"));
        dataExpLv.add(new CustomAdapterExpandableLV.Item(CustomAdapterExpandableLV.CHILD, "Audi"));
        dataExpLv.add(new CustomAdapterExpandableLV.Item(CustomAdapterExpandableLV.CHILD, "Aston Martin"));
        dataExpLv.add(new CustomAdapterExpandableLV.Item(CustomAdapterExpandableLV.CHILD, "BMW"));
        dataExpLv.add(new CustomAdapterExpandableLV.Item(CustomAdapterExpandableLV.CHILD, "Cadillac"));
        return dataExpLv;
    }

    public ArrayList<DataModelLV> lvData(String selectedCat1) {
        ArrayList<DataModelLV> input;
        input = new ArrayList<DataModelLV>();

        Sqlite sqliteObjMydata = new Sqlite(this.context);
        sqliteObjMydata.open();
        input = sqliteObjMydata.getSelectedCategoryLV(selectedCat1);
        sqliteObjMydata.close();
        return input;
    }

    public ArrayList<DataModelGrid> gridData() {
        ArrayList<DataModelGrid> data;
        data = new ArrayList<DataModelGrid>();
        for (int i = 0; i < MyData.nameArray.length; i++) {
            data.add(new DataModelGrid(
                    MyData.nameArray[i],
                    MyData.versionArray[i],
                    MyData.id_[i],
                    MyData.drawableArray[i]
            ));
        }
        return data;
    }

    public ArrayList<DataModelCartLV> getCartDetails() {
        Sqlite sqliteObjMydata = new Sqlite(context);
        ArrayList<DataModelCartLV> input;
        input = new ArrayList<DataModelCartLV>();
        //sqliteObjMydata.open();
        input = sqliteObjMydata.getCartDetails();
        //sqliteObjMydata.close();
        return input;
    }

    public ArrayList<DataModelGrid> getCatagory1Details() {
        ArrayList<DataModelGrid> input;
        input = new ArrayList<DataModelGrid>();
        Sqlite sqliteObjMydata = new Sqlite(this.context);
        sqliteObjMydata.open();
        input = sqliteObjMydata.getCatagory1Details();
        sqliteObjMydata.close();
        return input;
    }

    public String getOrderDetails() {
        JSONObject objJson = new JSONObject();
        JSONArray arrJson = new JSONArray();
        //build your jsonHere
        ArrayList<DataModelCartLV> input;
        input = new ArrayList<DataModelCartLV>();
        Sqlite sqliteObjMydata = new Sqlite(this.context);
        sqliteObjMydata.open();
        input = sqliteObjMydata.getCartDetails();
        sqliteObjMydata.close();
        for (int i = 0; i < input.size(); i++) {
            JSONObject eachArrObj = new JSONObject();
            JSONObject foodItemObj = new JSONObject();

            try {
                foodItemObj.put("type", input.get(i).getItemType());
                foodItemObj.put("id", input.get(i).getItemId());
                foodItemObj.put("name", input.get(i).getItemName());
                foodItemObj.put("price", input.get(i).getPrice());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                eachArrObj.put("itemId", input.get(i).getIndex());//put order id
                eachArrObj.put("food_item", foodItemObj);
                eachArrObj.put("quantity", input.get(i).getQuantity());
                Float finalPrice = input.get(i).getQuantity() * input.get(i).getPrice();
                eachArrObj.put("price", finalPrice);//each price multiplied quantity
                arrJson.put(eachArrObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        try {
            objJson.put("details", arrJson);
            objJson.put("intended_user", "Kitchen_Captain");
            objJson.put("roomno", "room_001");//getFrom tab credentials
            objJson.put("type", "FOOD_ORDER");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return objJson.toString();
    }

    public ArrayList<DataModelKitchenCaptainLV> getKCListviewDetails(){
        ArrayList<DataModelKitchenCaptainLV> input;
        input = new ArrayList<DataModelKitchenCaptainLV>();
        input.add(new DataModelKitchenCaptainLV("test1","veg",3,1));
        input.add(new DataModelKitchenCaptainLV("test1","veg",3,1));
        input.add(new DataModelKitchenCaptainLV("test1","veg",3,1));
        return input;
    }
}